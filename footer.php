<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after.  Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Boilerplate
 * @since Boilerplate 1.0
 */
?>		

		</section><!-- #main -->
		<footer class="footer" role="contentinfo">

			<a class="footer-name" href="<?php echo home_url( '/' ) ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
							<?php if(get_field('display_social_media_icons' , 'options')):?>
					<div class="social-media-icons">
						<?php if(get_field('sm_google' , 'options')):?>
							<a class="sm-google" href="<?php the_field('sm_google' , 'options');?>"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="30px" height="30px" viewBox="0 0 30 30" enable-background="new 0 0 30 30" xml:space="preserve">
<path class="sm-icon" d="M0,6.3V3.8c0-0.5,0.1-1,0.3-1.5c0.2-0.5,0.5-0.9,0.8-1.2c0.3-0.3,0.7-0.6,1.2-0.8C2.7,0.1,3.2,0,3.8,0h22.5
	c1,0,1.9,0.4,2.7,1.1C29.6,1.8,30,2.7,30,3.8v22.5c0,0.5-0.1,1-0.3,1.5c-0.2,0.5-0.5,0.8-0.8,1.2c-0.3,0.3-0.7,0.6-1.2,0.8
	c-0.5,0.2-1,0.3-1.5,0.3H14.8c0.3-0.5,0.5-1,0.7-1.6c0.2-0.5,0.3-1.1,0.3-1.7c0-0.9-0.1-1.7-0.4-2.4c-0.3-0.7-0.6-1.3-1.1-1.8
	c-0.4-0.5-0.9-1-1.4-1.4c-0.5-0.4-0.9-0.8-1.4-1.1c-0.4-0.4-0.8-0.7-1.1-1c-0.3-0.3-0.4-0.7-0.4-1.2c0-0.6,0.2-1.2,0.7-1.6
	c0.4-0.4,0.9-0.9,1.5-1.4c0.5-0.5,1-1.2,1.5-1.9c0.4-0.8,0.7-1.8,0.7-3.2c0-1-0.3-2-0.8-3.1c-0.5-1-1.2-1.8-2.1-2.4h2.6l2.6-1.5H8.3
	C6.6,2.8,5.1,3,3.7,3.5C2.3,4,1,4.9,0,6.3z M0,23.3v-8.6c0.7,0.9,1.5,1.5,2.4,1.9c0.9,0.4,1.9,0.6,3,0.6c0.2,0,0.5,0,0.7,0
	s0.5,0,0.7-0.1c-0.1,0.3-0.2,0.6-0.3,0.8c-0.1,0.2-0.1,0.5-0.1,0.8c0,0.5,0.1,1,0.4,1.5c0.2,0.5,0.5,0.9,0.8,1.3
	c-1.4,0.1-2.6,0.2-3.9,0.4C2.4,22.3,1.2,22.7,0,23.3z M0,26.7c0.2-0.7,0.5-1.4,1.1-1.9c0.5-0.5,1.2-0.9,1.9-1.2
	c0.7-0.3,1.5-0.5,2.2-0.6c0.8-0.1,1.5-0.2,2.2-0.2c0.2,0,0.4,0,0.6,0c0.2,0,0.4,0,0.6,0c0.5,0.4,1,0.7,1.5,1.1
	c0.5,0.4,1,0.8,1.5,1.2c0.4,0.4,0.8,0.9,1.1,1.5c0.3,0.5,0.4,1.1,0.4,1.8c0,0.6-0.1,1.2-0.4,1.7h-9c-0.9,0-1.8-0.3-2.5-1
	C0.6,28.4,0.2,27.6,0,26.7z M2,8.6C2,8.1,2.1,7.5,2.2,7c0.1-0.5,0.4-1,0.7-1.4c0.3-0.4,0.7-0.7,1.2-1c0.5-0.3,1-0.4,1.7-0.4
	C6.6,4.2,7.4,4.4,8,5c0.7,0.5,1.2,1.2,1.6,2c0.4,0.8,0.7,1.6,0.9,2.5c0.2,0.9,0.3,1.7,0.3,2.4c0,0.6-0.1,1.1-0.2,1.6
	c-0.1,0.5-0.3,1-0.6,1.3c-0.3,0.4-0.6,0.7-1.1,0.9c-0.5,0.2-1,0.3-1.6,0.3c-0.9,0-1.6-0.3-2.3-0.8c-0.7-0.5-1.2-1.1-1.7-1.9
	c-0.5-0.7-0.8-1.5-1-2.4C2.1,10.2,2,9.4,2,8.6z M16.7,9.5H21v4.4h2.1V9.5h4.4V7.4h-4.4V3H21v4.4h-4.4V9.5z"/>
</svg></a>
						<?php endif;?>
						<?php if(get_field('sm_facebook' , 'options')):?>
							<a class="sm-facebook" href="<?php the_field('sm_facebook' , 'options');?>"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="30px" height="30px" viewBox="0 0 30 30" enable-background="new 0 0 30 30" xml:space="preserve">
<path class="sm-icon" d="M0,26.2V3.8c0-0.5,0.1-1,0.3-1.5c0.2-0.5,0.5-0.9,0.8-1.2c0.3-0.3,0.7-0.6,1.2-0.8C2.7,0.1,3.2,0,3.7,0h22.5
	c1,0,1.9,0.4,2.7,1.1C29.6,1.8,30,2.7,30,3.8v22.5c0,0.5-0.1,1-0.3,1.5c-0.2,0.5-0.5,0.8-0.8,1.2c-0.3,0.3-0.7,0.6-1.2,0.8
	c-0.5,0.2-1,0.3-1.5,0.3H3.7c-0.5,0-1-0.1-1.4-0.3c-0.4-0.2-0.8-0.5-1.2-0.8c-0.3-0.3-0.6-0.7-0.8-1.2C0.1,27.3,0,26.8,0,26.2z
	 M3.9,19.1c1,1.4,2.3,2.5,3.9,3.3c1.5,0.8,3.2,1.1,5.1,1.1c1.3,0,2.5-0.2,3.6-0.5c1.2-0.3,2.2-0.8,3.1-1.4c0.9-0.6,1.7-1.3,2.4-2.2
	c0.7-0.9,1.2-1.8,1.5-2.8c1.1-0.1,1.9-0.4,2.5-1.1c0.2-0.2,0.2-0.4,0.1-0.6c-0.1-0.2-0.3-0.3-0.6-0.3h-0.1c0.3-0.3,0.5-0.6,0.6-0.9
	c0.1-0.3,0.1-0.5-0.2-0.6c-0.2-0.2-0.4-0.2-0.6,0c-0.1,0.1-0.3,0.1-0.6,0.2c-0.3,0.1-0.6,0.1-0.9,0.1c-0.1,0-0.1,0-0.1,0
	c0,0-0.1,0-0.1,0c0,0,0-0.1,0-0.1c0,0,0-0.1,0-0.1c-0.2-0.8-0.6-1.5-1.1-2.2c-0.5-0.6-1.1-1.1-1.8-1.4c0.1-0.1,0.1-0.1,0.1-0.2
	c0-0.1,0.1-0.1,0.1-0.2c0.1-0.2,0.1-0.4,0-0.6c0-0.1-0.1-0.2-0.2-0.3c-0.1-0.1-0.4-0.2-0.7-0.2c0-0.1-0.1-0.1-0.1-0.2
	c-0.2-0.2-0.3-0.2-0.5-0.2c-0.3,0.1-0.6,0.1-1,0.3l0,0c-0.2-0.1-0.4-0.1-0.6,0c-0.8,0.5-1.4,1.1-1.9,2c-0.5,0.8-1,1.7-1.3,2.6
	c-0.5-0.4-0.8-0.7-1.1-0.8c-0.8-0.5-1.6-0.9-2.5-1.2c-0.9-0.4-1.9-0.8-3-1.2c-0.2-0.1-0.3,0-0.5,0.1C7.1,9.5,7,9.6,7,9.8
	c0,0.4,0,0.7,0.2,1.1c0.1,0.4,0.4,0.8,0.8,1.2c-0.3,0.1-0.5,0.3-0.4,0.6c0.2,0.9,0.6,1.5,1.3,2L8.6,15c-0.2,0.2-0.2,0.4-0.1,0.6
	c0.1,0.2,0.2,0.4,0.5,0.7s0.7,0.6,1.3,0.8c-0.1,0.2-0.1,0.3-0.1,0.4c0,0.1,0,0.2,0,0.3c0.1,0.4,0.3,0.8,0.8,1
	c-0.5,0.3-1,0.5-1.5,0.7c-0.5,0.1-1.1,0.2-1.7,0.1c-0.6,0-1.1-0.2-1.6-0.4c-0.5-0.2-1-0.5-1.4-0.9c-0.1-0.1-0.2-0.2-0.4-0.2
	c-0.1,0-0.3,0.1-0.4,0.2C3.7,18.6,3.7,18.8,3.9,19.1z"/>
</svg></a>
						<?php endif;?>
						<?php if(get_field('sm_twitter' , 'options')):?>
							<a class="sm-twitter" href="<?php the_field('sm_twitter' , 'options');?>"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="30px" height="30px" viewBox="0 0 30 30" enable-background="new 0 0 30 30" xml:space="preserve">
<path class="sm-icon" d="M0,26.2V3.8c0-0.5,0.1-1,0.3-1.5c0.2-0.5,0.5-0.9,0.8-1.2s0.7-0.6,1.2-0.8C2.7,0.1,3.2,0,3.7,0h22.5c1,0,1.9,0.4,2.7,1.1
	C29.6,1.8,30,2.7,30,3.8v22.5c0,0.5-0.1,1-0.3,1.5c-0.2,0.5-0.5,0.8-0.8,1.2c-0.3,0.3-0.7,0.6-1.2,0.8c-0.5,0.2-1,0.3-1.4,0.3h-9.5
	V17.4h3.4c0.2,0,0.3-0.1,0.4-0.2c0.1-0.1,0.2-0.2,0.2-0.4l0.2-3.3c0-0.2-0.1-0.3-0.2-0.5c-0.1-0.1-0.3-0.2-0.4-0.2h-3.6v-1.4
	c0-0.5,0.1-0.9,0.2-1.1c0.1-0.2,0.5-0.3,1-0.3c0.3,0,0.7,0,1.1,0.1c0.4,0.1,0.8,0.1,1.1,0.2c0.1,0,0.2,0,0.3,0
	c0.1,0,0.2-0.1,0.2-0.1c0.1-0.1,0.2-0.2,0.3-0.4l0.4-3.2C21.5,6.3,21.3,6.1,21,6c-1.2-0.3-2.4-0.5-3.7-0.5c-3.9,0-5.9,1.9-5.9,5.7
	v1.6h-2c-0.4,0-0.6,0.2-0.6,0.6v3.3c0,0.2,0.1,0.3,0.2,0.4c0.1,0.1,0.3,0.2,0.4,0.2h2V30H3.7c-0.5,0-1-0.1-1.4-0.3
	c-0.5-0.2-0.8-0.5-1.2-0.8c-0.3-0.3-0.6-0.7-0.8-1.2C0.1,27.3,0,26.8,0,26.2z"/>
</svg></a>
						<?php endif;?>
					</div>
				<?php endif; ?>
		</footer><!-- footer -->
</div> <!-- container -->
		
<!-- Load libraries and plugins -->




<!-- Remove any navigation patterns below that arent used -->
<!-- // @javascript nav-toggle -->
<script src='<?php bloginfo('template_directory'); ?>/js/nav-patterns/responsive-nav.min.js'></script>

<!-- Load configuration and initialise JavaScripts. 
	 Look in config.js to see how to turn items on/off -->
<script src='<?php bloginfo('template_directory'); ?>/js/config.js'></script>
<script src='<?php bloginfo('template_directory'); ?>/js/main.js'></script>
<script src="<?php bloginfo('template_directory'); ?>/js/jquery.colorbox-min.js"></script>	

<script type="text/javascript">
	jQuery( document ).ready( function( $ ) {
		$(".gallery-item a").colorbox({rel:'thumbnail', photo:"true", maxWidth:"80%", maxHeight:"90%"});
	});
</script>		
<?php
	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */
	wp_footer();
?>
	</body>
</html>