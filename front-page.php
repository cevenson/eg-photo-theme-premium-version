<?php

get_header(); ?>

<?php if(get_field('home_page_layout_type') == "full_slider"): ?>
		
		<?php
		/*
		*  Create the Markup for full page slider
		*  This example will create the Markup for Flexslider (http://www.woothemes.com/flexslider/)
		*/
		 
		$images = get_field('full_slider');
		 
		if( $images ): ?>
		<!--  Load flexslider -->
		<script src='<?php bloginfo('template_directory'); ?>/js/vendor/jquery.flexslider-min.js'></script>




		<!-- Flexslider -->

		<script>

		jQuery(document).ready(function($) {
		$(window).load(function() {
		    $(".flexslider").flexslider({
		        animation: "fade",
		        directionNav: false,
		        controlNav: true,
		        slideshowSpeed: 4000,           //Integer: Set the speed of the slideshow cycling, in milliseconds
				animationSpeed: 1300,
		    });
		});
		});

		</script> 

		    <div id="full-slider" class="flexslider">
		     <?php if( have_rows('full_slider') ): ?>
		 
			<ul class="slides">
		 
			<?php while( have_rows('full_slider') ): the_row(); 
		 
				// vars
				$image = get_sub_field('full_slider_images');
				
		 
				?>
		 
				<li class="slide">
		 
				
		 
						<img class="bg" src="<?php echo $image['sizes']['full-slider']; ?>" alt="<?php echo $image['alt'] ?>" />
		 
					<?php if( $link ): ?>
						</a>
					<?php endif; ?>
		 
		 
				</li>
		 
			<?php endwhile; ?>
		 
			</ul>
		 
			<?php endif; ?>

				<?php if(get_field('full_slider_text')):?>
						<div class="full-slider-text">
								<?php the_field('full_slider_text');?>
						</div>
				<?php endif; ?>
			</div>
		<?php endif; ?>	

<?php endif; 
	//full slider
	?>
<?php if(get_field('home_page_layout_type') == "slider"): ?>
	<div id="home-page-slider">
		<?php
		/*
		*  Create the Markup for slider page with flexible content
		*  This example will create the Markup for Flexslider (http://www.woothemes.com/flexslider/)
		NEEDT TO CREATE CUSTOM IMAGE SIZE AND LOAD THAT ONE FOR SLIDES
		*/
		 
		$images = get_field('slider');
		 
		if( $images ): ?>
		<!--  Load flexslider -->
		<script src='<?php bloginfo('template_directory'); ?>/js/vendor/jquery.flexslider-min.js'></script>




		<!-- Flexslider -->

		<script>

		jQuery(document).ready(function($) {
		$(window).load(function() {
		    $(".flexslider").flexslider({
		        animation: "fade",
		        directionNav: true,
		        controlNav: false,
		        slideshowSpeed: 4000,           //Integer: Set the speed of the slideshow cycling, in milliseconds
				animationSpeed: 1300,
		    });
		});
		});

		</script> 

		    <div id="slider" class="flexslider">
		     <?php if( have_rows('slider') ): ?>
		 
			<ul class="slides">
		 
			<?php while( have_rows('slider') ): the_row(); 
		 
				// vars
				$image = get_sub_field('slider_images');
				$content = get_sub_field('slider_description');
				$link = get_sub_field('slider_link');
		 
				?>
		 
				<li class="slide">
		 
					<?php if( $link ): ?>
						<a href="<?php echo $link; ?>">
					<?php endif; ?>
		 
						<img src="<?php echo $image['sizes']['slider']; ?>" alt="<?php echo $image['alt'] ?>" />
		 
					<?php if( $link ): ?>
						</a>
					<?php endif; ?>
		 
					<?php if( $content ): ?>
				    <?php echo '<div class="slider-description"><p>' . $content .'</p></div>'; ?>
					<?php endif;?>
		 
				</li>
		 
			<?php endwhile; ?>
		 
			</ul>
		 
			<?php endif; ?>
			</div>
		<?php endif; ?>
		<?php
		 
		// check if the flexible content field has rows of data
		if( have_rows('slider_flexible_content') ):
		 
		     // loop through the rows of data
		    while ( have_rows('slider_flexible_content') ) : the_row();
		 
		        if( get_row_layout() == 'full_width_text' ):?>
		 
		        <section class="main-text">	<?php the_sub_field('full_width');?></section>
		 
		        <?php elseif( get_row_layout() == 'welcome_message' ): ?>
		 
		        	<h1 class="welcome-message"><?php the_sub_field('welcome_message_text');?></h1>
		 
		        <?php 

		        elseif( get_row_layout() == 'image_block' ): 
		 
		        	// check if the nested repeater field has rows of data
		        	if( have_rows('image_block_repeater') ):
		 
					 	echo '<section class="image-row">';
		 
					 	// loop through the rows of data
					    while ( have_rows('image_block_repeater') ) : the_row();
		 
							$image = get_sub_field('image');?>
							<div class="image-wrap">
		 
								<img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
									<p class="caption"><?php the_sub_field('image_caption');?></p>

		 					</div>
						<?php endwhile;
		 
						echo '</section>';
		 
					endif;

				elseif( get_row_layout() == 'blog_post_block' ): 

					// check if the nested repeater field has rows of data
		        	if( have_rows('blog_posts') ):?>
		        		<section class="blog-posts">
		        		
					   <?php while ( have_rows('blog_posts') ) : the_row(); // loop through the rows of data

			        		$post_object = get_sub_field('blog_post');
			 
								if( $post_object ): 
			 
								// override $post
								$post = $post_object;
								setup_postdata( $post ); 
			 
								?>
			    				<div class="blog-post">
			    					<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
			    					<p><?php the_excerpt(); ?></p>
			   				 	</div>
			   				<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
							<?php endif; 

						endwhile;?>
						</section>
					<?php endif; 	
		 
		        endif;


		    endwhile;
		 

		 
		endif;
		 
		?>
	</div>
<?php endif;
//slider ?>

<?php if(get_field('home_page_layout_type') == "masonry"): ?>
	<div id="home-page-masonry">

		<script>
		 jQuery( document ).ready( function( $ ) {          
			 $('#wall').masonry({
			    // options
			   // columnWidth: 200,
			    itemSelector : '.bricks',
			isAnimated: true,
			  animationOptions: {
			    duration: 500,
			    easing: 'linear',
			    queue: false}
			  });  
			  
			  //colourbox script
			  $(".thumbnail").colorbox({rel:'thumbnail', photo:"true", maxWidth:"80%", maxHeight:"90%"});
			  
			// or with jQuery
			var $container = $('#wall');
			// initialize Masonry after all images have loaded  
			$container.imagesLoaded( function() {
			  $container.masonry();
			});  


		});

		</script>
		<?php if (get_field('masonry_text')):?>
			<article class="masonry-text">
				<?php the_field('masonry_text');?>
			</article>
		<?php endif;?>
		 
		<?php $images = get_field('masonry');?>
		 
				<?php if ($images ): ?>
				    <div class="masonry-wrapper">
				        <ul id="wall">
				            <?php foreach( $images as $image ): ?>
				                <li class="bricks">
				                	<a class="thumbnail" href="<?php echo $image['sizes']['large']; ?>">
				                   		 <img src="<?php echo $image['sizes']['masonry-thumb']; ?>" alt="<?php echo $image['alt']; ?>" />
				                   		 <?php if($image['caption']):?>
				                   		 	<p class="caption"><?php echo $image['caption']; ?></p>
				                   		 <?php endif;?>
				                   	</a>
				                </li>
				            <?php endforeach; ?>
				        </ul>
				    </div>
				   <?php endif; ?>
	</div>
<?php endif;
//masonry ?>
<?php get_footer(); ?>