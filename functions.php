<?php
/**
 * Boilerplate functions and definitions
 *
 * Sets up the theme and provides some helper functions. Some helper functions
 * are used in the theme as custom template tags. Others are attached to action and
 * filter hooks in WordPress to change core functionality.
 *
 * The first function, boilerplate_setup(), sets up the theme by registering support
 * for various features in WordPress, such as post thumbnails, navigation menus, and the like.
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development and
 * http://codex.wordpress.org/Child_Themes), you can override certain functions
 * (those wrapped in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before the parent
 * theme's file, so the child theme functions would be used.
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are instead attached
 * to a filter or action hook. The hook can be removed by using remove_action() or
 * remove_filter() and you can attach your own function to the hook.
 *
 * We can remove the parent theme's hook only after it is attached, which means we need to
 * wait until setting up the child theme:
 *
 * <code>
 * add_action( 'after_setup_theme', 'my_child_theme_setup' );
 * function my_child_theme_setup() {
 *     // We are providing our own filter for excerpt_length (or using the unfiltered value)
 *     remove_filter( 'excerpt_length', 'boilerplate_excerpt_length' );
 *     ...
 * }
 * </code>
 *
 * For more information on hooks, actions, and filters, see http://codex.wordpress.org/Plugin_API.
 *
 * @package WordPress
 * @subpackage Boilerplate
 * @since Boilerplate 1.0
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * Used to set the width of images and content. Should be equal to the width the theme
 * is designed for, generally via the style.css stylesheet.
 */
if ( ! isset( $content_width ) )
	$content_width = 1100;

if ( ! function_exists( 'boilerplate_setup' ) ):
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which runs
	 * before the init hook. The init hook is too late for some features, such as indicating
	 * support post thumbnails.
	 *
	 * To override boilerplate_setup() in a child theme, add your own boilerplate_setup to your child theme's
	 * functions.php file.
	 *
	 * @uses add_theme_support() To add support for post thumbnails and automatic feed links.
	 * @uses register_nav_menus() To add support for navigation menus.
	 * @uses add_custom_background() To add support for a custom background.
	 * @uses add_editor_style() To style the visual editor.
	 * @uses load_theme_textdomain() For translation/localization support.
	 * @uses add_theme_support()/add_custom_image_header() To add support for a custom header.
	 * @uses register_default_headers() To register the default custom header images provided with the theme.
	 * @uses set_post_thumbnail_size() To set a custom post thumbnail size.
	 *
	 * @since Twenty Ten 1.0
	 */
	function boilerplate_setup() {

		// This theme styles the visual editor with editor-style.css to match the theme style.
		add_editor_style();

		// Uncomment if you choose to use post thumbnails; add the_post_thumbnail() wherever thumbnail should appear
		//add_theme_support( 'post-thumbnails' );

		// Add default posts and comments RSS feed links to head
		add_theme_support( 'automatic-feed-links' );

		// Make theme available for translation
		// Translations can be filed in the /languages/ directory
		load_theme_textdomain( 'boilerplate', get_template_directory() . '/languages' );

		$locale = get_locale();
		$locale_file = get_template_directory() . "/languages/$locale.php";
		if ( is_readable( $locale_file ) )
			require_once( $locale_file );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'primary' => __( 'Primary Navigation', 'boilerplate' ),
		) );



	}
endif;
add_action( 'after_setup_theme', 'boilerplate_setup' );

if ( ! function_exists( 'boilerplate_admin_header_style' ) ) :
	/**
	 * Styles the header image displayed on the Appearance > Header admin panel.
	 *
	 * Referenced via add_theme_support()/add_custom_image_header() in boilerplate_setup().
	 *
	 * @since Twenty Ten 1.0
	 */
	function boilerplate_admin_header_style() {
	?>
	<style type="text/css">
	/* Shows the same border as on front end */
	#headimg {
		border-bottom: 1px solid #000;
		border-top: 4px solid #000;
	}
	/* If NO_HEADER_TEXT is false, you would style the text with these selectors:
		#headimg #name { }
		#headimg #desc { }
	*/
	</style>
	<?php
	}
endif;

if ( ! function_exists( 'boilerplate_filter_wp_title' ) ) :
	/**
	 * Makes some changes to the <title> tag, by filtering the output of wp_title().
	 *
	 * If we have a site description and we're viewing the home page or a blog posts
	 * page (when using a static front page), then we will add the site description.
	 *
	 * If we're viewing a search result, then we're going to recreate the title entirely.
	 * We're going to add page numbers to all titles as well, to the middle of a search
	 * result title and the end of all other titles.
	 *
	 * The site title also gets added to all titles.
	 *
	 * @since Twenty Ten 1.0
	 *
	 * @param string $title Title generated by wp_title()
	 * @param string $separator The separator passed to wp_title(). Twenty Ten uses a
	 * 	vertical bar, "|", as a separator in header.php.
	 * @return string The new title, ready for the <title> tag.
	 */
	function boilerplate_filter_wp_title( $title, $separator ) {
		// Don't affect wp_title() calls in feeds.
		if ( is_feed() )
			return $title;

		// The $paged global variable contains the page number of a listing of posts.
		// The $page global variable contains the page number of a single post that is paged.
		// We'll display whichever one applies, if we're not looking at the first page.
		global $paged, $page;

		if ( is_search() ) {
			// If we're a search, let's start over:
			$title = sprintf( __( 'Search results for %s', 'boilerplate' ), '"' . get_search_query() . '"' );
			// Add a page number if we're on page 2 or more:
			if ( $paged >= 2 )
				$title .= " $separator " . sprintf( __( 'Page %s', 'boilerplate' ), $paged );
			// Add the site name to the end:
			$title .= " $separator " . get_bloginfo( 'name', 'display' );
			// We're done. Let's send the new title back to wp_title():
			return $title;
		}

		// Otherwise, let's start by adding the site name to the end:
		$title .= get_bloginfo( 'name', 'display' );

		// If we have a site description and we're on the home/front page, add the description:
		$site_description = get_bloginfo( 'description', 'display' );
		if ( $site_description && ( is_home() || is_front_page() ) )
			$title .= " $separator " . $site_description;

		// Add a page number if necessary:
		if ( $paged >= 2 || $page >= 2 )
			$title .= " $separator " . sprintf( __( 'Page %s', 'boilerplate' ), max( $paged, $page ) );

		// Return the new title to wp_title():
		return $title;
	}
endif;
add_filter( 'wp_title', 'boilerplate_filter_wp_title', 10, 2 );

if ( ! function_exists( 'boilerplate_page_menu_args' ) ) :
	/**
	 * Get our wp_nav_menu() fallback, wp_page_menu(), to show a home link.
	 *
	 * To override this in a child theme, remove the filter and optionally add
	 * your own function tied to the wp_page_menu_args filter hook.
	 *
	 * @since Twenty Ten 1.0
	 */
	function boilerplate_page_menu_args( $args ) {
		$args['show_home'] = true;
		return $args;
	}
endif;
add_filter( 'wp_page_menu_args', 'boilerplate_page_menu_args' );

if ( ! function_exists( 'boilerplate_excerpt_length' ) ) :
	/**
	* Sets the post excerpt length to 40 characters.
	*
	* To override this length in a child theme, remove the filter and add your own
	* function tied to the excerpt_length filter hook.
	*
	* @since Twenty Ten 1.0
	* @return int
	*/
   function boilerplate_excerpt_length( $length ) {
	   return 40;
   }
endif;
add_filter( 'excerpt_length', 'boilerplate_excerpt_length' );

if ( ! function_exists( 'boilerplate_continue_reading_link' ) ) :
	/**
	 * Returns a "Continue Reading" link for excerpts
	 *
	 * @since Twenty Ten 1.0
	 * @return string "Continue Reading" link
	 */
	function boilerplate_continue_reading_link() {
		return ' <a href="'. get_permalink() . '">' . __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'boilerplate' ) . '</a>';
	}
endif;

if ( ! function_exists( 'boilerplate_auto_excerpt_more' ) ) :
	/**
	 * Replaces "[...]" (appended to automatically generated excerpts) with an ellipsis and boilerplate_continue_reading_link().
	 *
	 * To override this in a child theme, remove the filter and add your own
	 * function tied to the excerpt_more filter hook.
	 *
	 * @since Twenty Ten 1.0
	 * @return string An ellipsis
	 */
	function boilerplate_auto_excerpt_more( $more ) {
		return ' &hellip;' . boilerplate_continue_reading_link();
	}
endif;
add_filter( 'excerpt_more', 'boilerplate_auto_excerpt_more' );

if ( ! function_exists( 'boilerplate_custom_excerpt_more' ) ) :
	/**
	 * Adds a pretty "Continue Reading" link to custom post excerpts.
	 *
	 * To override this link in a child theme, remove the filter and add your own
	 * function tied to the get_the_excerpt filter hook.
	 *
	 * @since Twenty Ten 1.0
	 * @return string Excerpt with a pretty "Continue Reading" link
	 */
	function boilerplate_custom_excerpt_more( $output ) {
		if ( has_excerpt() && ! is_attachment() ) {
			$output .= boilerplate_continue_reading_link();
		}
		return $output;
	}
endif;
add_filter( 'get_the_excerpt', 'boilerplate_custom_excerpt_more' );

if ( ! function_exists( 'boilerplate_remove_gallery_css' ) ) :/**
	 * Remove inline styles printed when the gallery shortcode is used.
	 *
	 * Galleries are styled by the theme in Twenty Ten's style.css.
	 *
	 * @since Twenty Ten 1.0
	 * @return string The gallery style filter, with the styles themselves removed.
	 */
	function boilerplate_remove_gallery_css( $css ) {
		return preg_replace( "#<style type='text/css'>(.*?)</style>#s", '', $css );
	}
endif;
add_filter( 'gallery_style', 'boilerplate_remove_gallery_css' );

if ( ! function_exists( 'boilerplate_comment' ) ) :
	/**
	 * Template for comments and pingbacks.
	 *
	 * To override this walker in a child theme without modifying the comments template
	 * simply create your own boilerplate_comment(), and that function will be used instead.
	 *
	 * Used as a callback by wp_list_comments() for displaying the comments.
	 *
	 * @since Twenty Ten 1.0
	 */
	function boilerplate_comment( $comment, $args, $depth ) {
		$GLOBALS['comment'] = $comment;
		switch ( $comment->comment_type ) :
			case '' :
		?>
		<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
			<article id="comment-<?php comment_ID(); ?>">
				<div class="comment-author vcard">
					<?php echo get_avatar( $comment, 40 ); ?>
					<?php printf( __( '%s <span class="says">says:</span>', 'boilerplate' ), sprintf( '<cite class="fn">%s</cite>', get_comment_author_link() ) ); ?>
				</div><!-- .comment-author .vcard -->
				<?php if ( $comment->comment_approved == '0' ) : ?>
					<em><?php _e( 'Your comment is awaiting moderation.', 'boilerplate' ); ?></em>
					<br />
				<?php endif; ?>
				<footer class="comment-meta commentmetadata"><a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>">
					<?php
						/* translators: 1: date, 2: time */
						printf( __( '%1$s at %2$s', 'boilerplate' ), get_comment_date(),  get_comment_time() ); ?></a><?php edit_comment_link( __( '(Edit)', 'boilerplate' ), ' ' );
					?>
				</footer><!-- .comment-meta .commentmetadata -->
				<div class="comment-body"><?php comment_text(); ?></div>
				<div class="reply">
					<?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
				</div><!-- .reply -->
			</article><!-- #comment-##  -->
		<?php
				break;
			case 'pingback'  :
			case 'trackback' :
		?>
		<li class="post pingback">
			<p><?php _e( 'Pingback:', 'boilerplate' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __('(Edit)', 'boilerplate'), ' ' ); ?></p>
		<?php
				break;
		endswitch;
	}
endif;

if ( ! function_exists( 'boilerplate_widgets_init' ) ) :
	/**
	 * Register widgetized areas, including two sidebars and four widget-ready columns in the footer.
	 *
	 * To override boilerplate_widgets_init() in a child theme, remove the action hook and add your own
	 * function tied to the init hook.
	 *
	 * @since Twenty Ten 1.0
	 * @uses register_sidebar
	 */
	function boilerplate_widgets_init() {
		// Area 1, located at the top of the sidebar.
		register_sidebar( array(
			'name' => __( 'Primary Widget Area', 'boilerplate' ),
			'id' => 'primary-widget-area',
			'description' => __( 'The primary widget area', 'boilerplate' ),
			'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
			'after_widget' => '</li>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>',
		) );

		// Area 2, located below the Primary Widget Area in the sidebar. Empty by default.
		register_sidebar( array(
			'name' => __( 'Secondary Widget Area', 'boilerplate' ),
			'id' => 'secondary-widget-area',
			'description' => __( 'The secondary widget area', 'boilerplate' ),
			'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
			'after_widget' => '</li>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>',
		) );


	}
endif;
add_action( 'widgets_init', 'boilerplate_widgets_init' );

if ( ! function_exists( 'boilerplate_remove_recent_comments_style' ) ) :
	/**
	 * Removes the default styles that are packaged with the Recent Comments widget.
	 *
	 * To override this in a child theme, remove the filter and optionally add your own
	 * function tied to the widgets_init action hook.
	 *
	 * @since Twenty Ten 1.0
	 */
	function boilerplate_remove_recent_comments_style() {
		global $wp_widget_factory;
		remove_action( 'wp_head', array( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style' ) );
	}
endif;
add_action( 'widgets_init', 'boilerplate_remove_recent_comments_style' );

if ( ! function_exists( 'boilerplate_posted_on' ) ) :
	/**
	 * Prints HTML with meta information for the current post—date/time and author.
	 *
	 * @since Twenty Ten 1.0
	 */
	function boilerplate_posted_on() {
		// BP: slight modification to Twenty Ten function, converting single permalink to multi-archival link
		// Y = 2012
		// F = September
		// m = 01–12
		// j = 1–31
		// d = 01–31
		printf( __( '<span class="%1$s icon-blog-icons-calendar">Posted on</span> <span class="entry-date">%2$s %3$s %4$s</span> <span class="meta-sep">by</span> %5$s', 'boilerplate' ),
			// %1$s = container class
			'meta-prep meta-prep-author',
			// %2$s = month: /yyyy/mm/
			sprintf( '<a href="%1$s" title="%2$s" rel="bookmark">%3$s</a>',
				home_url() . '/' . get_the_date( 'Y' ) . '/' . get_the_date( 'm' ) . '/',
				esc_attr( 'View Archives for ' . get_the_date( 'F' ) . ' ' . get_the_date( 'Y' ) ),
				get_the_date( 'F' )
			),
			// %3$s = day: /yyyy/mm/dd/
			sprintf( '<a href="%1$s" title="%2$s" rel="bookmark">%3$s</a>',
				home_url() . '/' . get_the_date( 'Y' ) . '/' . get_the_date( 'm' ) . '/' . get_the_date( 'd' ) . '/',
				esc_attr( 'View Archives for ' . get_the_date( 'F' ) . ' ' . get_the_date( 'j' ) . ' ' . get_the_date( 'Y' ) ),
				get_the_date( 'j' )
			),
			// %4$s = year: /yyyy/
			sprintf( '<a href="%1$s" title="%2$s" rel="bookmark">%3$s</a>',
				home_url() . '/' . get_the_date( 'Y' ) . '/',
				esc_attr( 'View Archives for ' . get_the_date( 'Y' ) ),
				get_the_date( 'Y' )
			),
			// %5$s = author vcard
			sprintf( '<span class="author vcard"><a class="url fn n" href="%1$s" title="%2$s">%3$s</a></span>',
				get_author_posts_url( get_the_author_meta( 'ID' ) ),
				sprintf( esc_attr__( 'View all posts by %s', 'boilerplate' ), get_the_author() ),
				get_the_author()
			)
		);
	}
endif;

if ( ! function_exists( 'boilerplate_posted_in' ) ) :
	/**
	 * Prints HTML with meta information for the current post (category, tags and permalink).
	 *
	 * @since Twenty Ten 1.0
	 */
	function boilerplate_posted_in() {
		// Retrieves tag list of current post, separated by commas.
		$tag_list = get_the_tag_list( '', ', ' );
		if ( $tag_list ) {
			$posted_in = __( '<span class="icon-blog-icons-folder">This entry was posted in</span> %1$s <span class="icon-blog-icons-tag">and tagged</span> %2$s. <span class="icon-blog-icons-ribbon">Bookmark the</span> <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'boilerplate' );
		} elseif ( is_object_in_taxonomy( get_post_type(), 'category' ) ) {
			$posted_in = __( '<span class="icon-blog-icons-folder">This entry was posted in</span> %1$s. <span class="icon-blog-icons-ribbon">Bookmark the</span> <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'boilerplate' );
		} else {
			$posted_in = __( '<span class="icon-blog-icons-ribbon">Bookmark the</span> <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'boilerplate' );
		}
		// Prints the string, replacing the placeholders.
		printf(
			$posted_in,
			get_the_category_list( ', ' ),
			$tag_list,
			get_permalink(),
			the_title_attribute( 'echo=0' )
		);
	}
endif;
/*	End original TwentyTen functions (from Starkers Theme, renamed into this namespace) */

/*	Begin Boilerplate */
	// Add Admin
	//require_once(get_template_directory() . '/boilerplate-admin/admin-menu.php');

	// remove version info from head and feeds (http://digwp.com/2009/07/remove-wordpress-version-number/)
	if ( ! function_exists( 'boilerplate_complete_version_removal' ) ) :
		function boilerplate_complete_version_removal() {
			return '';
		}
	endif;
	add_filter('the_generator', 'boilerplate_complete_version_removal');

	// add thumbnail support
	if ( function_exists( 'add_theme_support' ) ) :
		add_theme_support( 'post-thumbnails' );
	endif;

/**
 * Change default fields, add placeholder and change type attributes.
 * @param  array $fields
 * @return array
 * from: http://wordpress.stackexchange.com/questions/62742/add-placeholder-attribute-to-comment-form-fields
 */
	function boilerplate_comment_input_placeholders( $fields ) {

		$fields['author'] = str_replace(
			'<input',
			'<input placeholder="'
			/* Replace 'theme_text_domain' with your theme’s text domain.
			 * I use _x() here to make your translators life easier. :)
			 * See http://codex.wordpress.org/Function_Reference/_x
			 */
				. _x(
					'Your Name',
					'comment form placeholder',
					'boilerplate'
					)
				. '"',
			$fields['author']
		);
		$fields['email'] = str_replace(
			'<input id="email" name="email" type="text"',
			/* We use a proper type attribute to make use of the browser’s
			 * validation, and to get the matching keyboard on smartphones.
			 */
			'<input type="email" placeholder="contact@example.com"  id="email" name="email"',
			$fields['email']
		);
		$fields['url'] = str_replace(
			'<input id="url" name="url" type="text"',
			// Again: a better 'type' attribute value.
			'<input placeholder="http://example.com/" id="url" name="url" type="url"',
			$fields['url']
		);
		return $fields;
	}
	add_filter( 'comment_form_default_fields', 'boilerplate_comment_input_placeholders' );
	// ATG: added to customize <textarea> also
	function boilerplate_comment_field_placeholder( $fields ) {
		$fields = str_replace(
			'<textarea',
			'<textarea placeholder="'
			/* Replace 'theme_text_domain' with your theme’s text domain.
			 * I use _x() here to make your translators life easier. :)
			 * See http://codex.wordpress.org/Function_Reference/_x
			 */
				. _x(
					'Your Comment',
					'comment form placeholder',
					'boilerplate'
					)
				. '"',
			$fields
		);
		return $fields;
	}
	add_filter( 'comment_form_field_comment', 'boilerplate_comment_field_placeholder' );

/*	End Boilerplate */

function my_init_method() {
wp_enqueue_script('jquery');
}
add_action('wp_enqueue_scripts', 'my_init_method');

// Pagination Function

function fb_paging_bar( $args = array() ) {
	
	$defaults = array(
		'range'           => 4,
		'custom_query'    => FALSE,
		'previous_string' => __( '«««', FB_GREYFOTO_TEXTDOMAIN ),
		'next_string'     => __( '»»»', FB_GREYFOTO_TEXTDOMAIN ),
		'view_fp'         => TRUE,
		'view_lp'         => TRUE,
		'before_output'   => '<ul>',
		'after_output'    => '</ul>'
	);
	
	$args = wp_parse_args( 
		$args, 
		apply_filters( 'fb_paging_bar_defaults', $defaults )
	);
	
	$args['range'] = (int) $args['range'] - 1;
	if ( !$args['custom_query'] )
		$args['custom_query'] = @$GLOBALS['wp_query'];
	$count = (int) $args['custom_query']->max_num_pages;
	$page  = intval( get_query_var( 'paged' ) );
	$ceil  = ceil( $args['range'] / 2 );
	
	if ( $count <= 1 )
		return FALSE;
	
	if ( !$page )
		$page = 1;
	
	if ( $count > $args['range'] ) {
		if ( $page <= $args['range'] ) {
			$min = 1;
			$max = $args['range'] + 1;
		} elseif ( $page >= ($count - $ceil) ) {
			$min = $count - $args['range'];
			$max = $count;
		} elseif ( $page >= $args['range'] && $page < ($count - $ceil) ) {
			$min = $page - $ceil;
			$max = $page + $ceil;
		}
	} else {
		$min = 1;
		$max = $count;
	}
	
	$echo = '';
	$previous = intval($page) - 1;
	$previous = esc_attr( get_pagenum_link($previous) );
	if ( $previous && (1 != $page) )
		$echo .= '<li><a href="' . $previous . '" title="' . __( 'previous', FB_GREYFOTO_TEXTDOMAIN) . '">' . $args['previous_string'] . '</a></li>';
	$firstpage = esc_attr( get_pagenum_link(1) );
	
	if ( $args['view_fp'] && $firstpage && (1 != $page) )
		$echo .= '<li><a href="' . $firstpage . '">' . __( 'First', FB_GREYFOTO_TEXTDOMAIN ) . '</a></li>';
	
	if ( !empty($min) && !empty($max) ) {
		for( $i = $min; $i <= $max; $i++ ) {
			if ($page == $i) {
				$echo .= '<li><span class="active">' . str_pad( (int)$i, 2, '0', STR_PAD_LEFT ) . '</span></li>';
			} else {
				$echo .= sprintf( '<li><a href="%s">%002d</a>', esc_attr( get_pagenum_link($i) ), $i );
			}
		}
	}
	
	if ($args['view_lp']) {
		$lastpage = esc_attr( get_pagenum_link($count) );
		if ( $lastpage && ($count != $page) ) {
			$count = str_pad( (int)$count, 2, '0', STR_PAD_LEFT );
			$echo .= '<a href="' . $lastpage . '">' . __( 'Last', FB_GREYFOTO_TEXTDOMAIN ) . '(' . $count . ')' . '</a></li>';
		}
	}
	
	$next = intval($page) + 1;
	$next = esc_attr( get_pagenum_link($next) );
	if ($next && ($count != $page) )
		$echo .= '<li><a href="' . $next . '" title="' . __( 'next', FB_GREYFOTO_TEXTDOMAIN) . '">' . $args['next_string'] . '</a></li>';
	
	if ( isset($echo) )
		echo $args['before_output'] . $echo . $args['after_output'];
}

// Load masonry
function mason_script() {
    wp_enqueue_script( 'jquery-masonry' );
}
add_action( 'wp_enqueue_scripts', 'mason_script' );

// Custom image sizes
if ( function_exists( 'add_image_size' ) ) { 
	add_image_size( 'masonry-thumb', 300, 450);
	add_image_size( 'full-slider', 1600, 1067);
	add_image_size( 'slider', 1400, 620, true);
}

// Set thumbnail size
update_option('thumbnail_size_w', 300);
update_option('thumbnail_size_h', 300);
//update_option('thumbnail_crop', 1);

// get rid of website field in comments
add_filter('comment_form_default_fields', 'unset_url_field');
function unset_url_field($fields){
if(isset($fields['url']))
unset($fields['url']);
return $fields;
}



/**
 * Registers options with the Theme Customizer
 *
 * @param      object    $wp_customize    The WordPress Theme Customizer
 * @package    tcx
 * @since      0.2.0
 * @version    0.2.0
 */
function tcx_register_theme_customizer( $wp_customize ) {

	$wp_customize->add_setting(
		'tcx_link_color',
		array(
			'default'     => '#7a7a7a',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'link_color',
			array(
			    'label'      => __( 'Link Color', 'tcx' ),
			    'section'    => 'colors',
			    'settings'   => 'tcx_link_color'
			)
		)
	);

	$wp_customize->add_setting(
		'tcx_link_color_hover',
		array(
			'default'     => '#565656',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'link_color_hover',
			array(
			    'label'      => __( 'Link Hover Color', 'tcx' ),
			    'section'    => 'colors',
			    'settings'   => 'tcx_link_color_hover'
			)
		)
	);

	$wp_customize->add_setting(
		'tcx_background_color',
		array(
			'default'     => '#ffffff',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'background_color',
			array(
			    'label'      => __( 'Background Color', 'tcx' ),
			    'section'    => 'colors',
			    'settings'   => 'tcx_background_color'
			)
		)
	);

	$wp_customize->add_setting(
		'tcx_nav_color',
		array(
			'default'     => '#b7b7b7',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'nav_color',
			array(
			    'label'      => __( 'Navigation Color', 'tcx' ),
			    'section'    => 'colors',
			    'settings'   => 'tcx_nav_color'
			)
		)
	);

		$wp_customize->add_setting(
		'tcx_nav_link_color',
		array(
			'default'     => '#ffffff',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'nav_link_color',
			array(
			    'label'      => __( 'Navigation Link Color', 'tcx' ),
			    'section'    => 'colors',
			    'settings'   => 'tcx_nav_link_color'
			)
		)
	);

		$wp_customize->add_setting(
		'tcx_nav_link_hover_color',
		array(
			'default'     => '#000000',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'nav_link_hover_color',
			array(
			    'label'      => __( 'Navigation Link Hover Color', 'tcx' ),
			    'section'    => 'colors',
			    'settings'   => 'tcx_nav_link_hover_color'
			)
		)
	);

} // end tcx_register_theme_customizer
add_action( 'customize_register', 'tcx_register_theme_customizer' );


function tcx_customizer_css() {
?>
	 <style type="text/css">

	     a { color: <?php echo get_theme_mod( 'tcx_link_color' ); ?>; }
	     a:visited { color: <?php echo get_theme_mod( 'tcx_link_color' ); ?>; }
	     a:hover { color: <?php echo get_theme_mod( 'tcx_link_color_hover' ); ?>; }
	     .sm-icon { fill: <?php echo get_theme_mod( 'tcx_link_color' ); ?>;}
	     body { background-color: <?php echo get_theme_mod( 'tcx_background_color' ); ?>; }
	     #navigation-toggle  {background-color: <?php echo get_theme_mod( 'tcx_nav_color' ); ?>; }
	     #nav-toggle {background-color: <?php echo get_theme_mod( 'tcx_nav_color' ); ?>; }
	     #navigation-toggle a {color: <?php echo get_theme_mod( 'tcx_nav_link_color' ); ?>; }
	     #navigation-toggle a:hover {color: <?php echo get_theme_mod( 'tcx_nav_link_hover_color' ); ?>; }
	     #navigation-toggle li ul li {background-color: <?php echo get_theme_mod( 'tcx_nav_color' ); ?>; }

	 </style>
<?php
}
add_action( 'wp_head', 'tcx_customizer_css' );


//add_action( 'customize_preview_init', 'tcx_customizer_live_preview' );

/** Plugin Installer
 * Include the TGM_Plugin_Activation class.
 */
require_once dirname( __FILE__ ) . '/class-tgm-plugin-activation.php';

add_action( 'tgmpa_register', 'my_theme_register_required_plugins' );
/**
 * Register the required plugins for this theme.
 *
 * In this example, we register two plugins - one included with the TGMPA library
 * and one from the .org repo.
 *
 * The variable passed to tgmpa_register_plugins() should be an array of plugin
 * arrays.
 *
 * This function is hooked into tgmpa_init, which is fired within the
 * TGM_Plugin_Activation class constructor.
 */
function my_theme_register_required_plugins() {

    /**
     * Array of plugin arrays. Required keys are name and slug.
     * If the source is NOT from the .org repo, then source is also required.
     */
    $plugins = array(

        // This is an example of how to include a plugin pre-packaged with a theme.
        array(
            'name'               => 'Easy Google Fonts', // The plugin name.
            'slug'               => 'easy-google-fonts', // The plugin slug (typically the folder name).
            'source'             => get_stylesheet_directory() . '/plugins/easy-google-fonts.zip', // The plugin source.
            'required'           => true, // If false, the plugin is only 'recommended' instead of required.
            'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher.
            'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
            'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
            'external_url'       => '', // If set, overrides default API URL and points to an external URL.
        ),

        // This is an example of how to include a plugin pre-packaged with a theme.
        array(
            'name'               => 'Contact Form 7', // The plugin name.
            'slug'               => 'contact-form-7', // The plugin slug (typically the folder name).
            'source'             => get_stylesheet_directory() . '/plugins/contact-form-7.3.8.1.zip', // The plugin source.
            'required'           => false, // If false, the plugin is only 'recommended' instead of required.
            'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher.
            'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
            'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
            'external_url'       => '', // If set, overrides default API URL and points to an external URL.
        ),

                // This is an example of how to include a plugin pre-packaged with a theme.
        array(
            'name'               => 'Regenerate Thumbnails', // The plugin name.
            'slug'               => 'regenerate-thumbnails', // The plugin slug (typically the folder name).
            'source'             => get_stylesheet_directory() . '/plugins/regenerate-thumbnails.zip', // The plugin source.
            'required'           => false, // If false, the plugin is only 'recommended' instead of required.
            'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher.
            'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
            'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
            'external_url'       => '', // If set, overrides default API URL and points to an external URL.
        ),

                        // This is an example of how to include a plugin pre-packaged with a theme.
        array(
            'name'               => 'ACF Pro', // The plugin name.
            'slug'               => 'advanced-custom-fields-pro', // The plugin slug (typically the folder name).
            'source'             => get_stylesheet_directory() . '/plugins/advanced-custom-fields-pro.zip', // The plugin source.
            'required'           => true, // If false, the plugin is only 'recommended' instead of required.
            'version'            => '5.0.3', // E.g. 1.0.0. If set, the active plugin must be this version or higher.
            'force_activation'   => true, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
            'force_deactivation' => true, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
            'external_url'       => '', // If set, overrides default API URL and points to an external URL.
        ),

                                // This is an example of how to include a plugin pre-packaged with a theme.
        array(
            'name'               => 'WP Help', // The plugin name.
            'slug'               => 'wp-help', // The plugin slug (typically the folder name).
            'source'             => get_stylesheet_directory() . '/plugins/wp-help.zip', // The plugin source.
            'required'           => false, // If false, the plugin is only 'recommended' instead of required.
            'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher.
            'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
            'force_deactivation' => true, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
            'external_url'       => '', // If set, overrides default API URL and points to an external URL.
        ),

    );

    /**
     * Array of configuration settings. Amend each line as needed.
     * If you want the default strings to be available under your own theme domain,
     * leave the strings uncommented.
     * Some of the strings are added into a sprintf, so see the comments at the
     * end of each line for what each argument will be.
     */
    $config = array(
        'id'           => 'tgmpa',                 // Unique ID for hashing notices for multiple instances of TGMPA.
        'default_path' => '',                      // Default absolute path to pre-packaged plugins.
        'menu'         => 'tgmpa-install-plugins', // Menu slug.
        'has_notices'  => true,                    // Show admin notices or not.
        'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
        'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => false,                   // Automatically activate plugins after installation or not.
        'message'      => '',                      // Message to output right before the plugins table.
        'strings'      => array(
            'page_title'                      => __( 'Install Required Plugins', 'tgmpa' ),
            'menu_title'                      => __( 'Install Plugins', 'tgmpa' ),
            'installing'                      => __( 'Installing Plugin: %s', 'tgmpa' ), // %s = plugin name.
            'oops'                            => __( 'Something went wrong with the plugin API.', 'tgmpa' ),
            'notice_can_install_required'     => _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.', 'tgmpa' ), // %1$s = plugin name(s).
            'notice_can_install_recommended'  => _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.', 'tgmpa' ), // %1$s = plugin name(s).
            'notice_cannot_install'           => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.', 'tgmpa' ), // %1$s = plugin name(s).
            'notice_can_activate_required'    => _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.', 'tgmpa' ), // %1$s = plugin name(s).
            'notice_can_activate_recommended' => _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.', 'tgmpa' ), // %1$s = plugin name(s).
            'notice_cannot_activate'          => _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.', 'tgmpa' ), // %1$s = plugin name(s).
            'notice_ask_to_update'            => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.', 'tgmpa' ), // %1$s = plugin name(s).
            'notice_cannot_update'            => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.', 'tgmpa' ), // %1$s = plugin name(s).
            'install_link'                    => _n_noop( 'Begin installing plugin', 'Begin installing plugins', 'tgmpa' ),
            'activate_link'                   => _n_noop( 'Begin activating plugin', 'Begin activating plugins', 'tgmpa' ),
            'return'                          => __( 'Return to Required Plugins Installer', 'tgmpa' ),
            'plugin_activated'                => __( 'Plugin activated successfully.', 'tgmpa' ),
            'complete'                        => __( 'All plugins installed and activated successfully. %s', 'tgmpa' ), // %s = dashboard link.
            'nag_type'                        => 'updated' // Determines admin notice type - can only be 'updated', 'update-nag' or 'error'.
        )
    );

    tgmpa( $plugins, $config );
}

if(function_exists('acf_add_options_page')) { 
 
	acf_add_options_page();
 
}



add_shortcode( 'gallery', 'custom_gallery_shortcode' );

function custom_gallery_shortcode( $attr = array(), $content = '' )
{
        $attr['itemtag']        = "li";
        $attr['icontag']        = "";
        $attr['captiontag']     = "p";

        // Run the native gallery shortcode callback:    
        $html = gallery_shortcode( $attr );

        // Remove all tags except a, img,li, p
        $html = strip_tags( $html, '<a><img><li><p>' );

        // Some trivial replacements:
        $from = array(  
            "class='gallery-item'", 
            "class='gallery-icon landscape'", 
            'class="attachment-thumbnail"',
            'a href=', 
        );              
        $to = array( 
            'class="gallery-item"',
            '',
            'class="attachment-thumbnail"', 
            'a class="lightbox" href=', 
        );
        $html = str_replace( $from, $to, $html );

        // Remove width/height attributes:
        $html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );

        // Wrap the output in ul tags:
        $html = sprintf( '<ul class="gallery">%s</ul>', $html );

        return $html;
}

function button($atts, $content = null) {
 extract( shortcode_atts( array(
          'url' => '#'
), $atts ) );
return '<a href="'.$url.'" class="btn"><span>' . do_shortcode($content) . '</span></a>';
}
add_shortcode('button', 'button');

function remove_footer_admin () {
echo 'Fueled by <a href="http://www.wordpress.org" target="_blank">WordPress</a> | Designed by <a href="http://evensongraphics.co.nz" target="_blank">Evenson Graphics</a></p>';
}
add_filter('admin_footer_text', 'remove_footer_admin');

add_action('wp_dashboard_setup', 'my_custom_dashboard_widgets');
 
function my_custom_dashboard_widgets() {
global $wp_meta_boxes;

wp_add_dashboard_widget('custom_help_widget', 'Theme Support', 'custom_dashboard_help');
}

function custom_dashboard_help() {
echo '<p>Welcome to your website!</br>Need help? Try looking in the Publishing Help section in the left hand menu.</br> You can contact Evenson Graphics <a href="mailto:cavell@evensongraphics.co.nz">here</a>. For website tips visit <a href="http://www.evensongraphics.co.nz/category/blog/" target="_blank">my blog</a></p>
<p>Evenson Graphics Web and Print Design: 021 780 525';
}

function remove_dashboard_meta() {
        remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_primary', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
        //remove_meta_box( 'dashboard_quick_press', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
        remove_meta_box( 'dashboard_activity', 'dashboard', 'normal');//since 3.8
}
add_action( 'admin_init', 'remove_dashboard_meta' );


/**
 * Hide ACF menu item from the admin menu
 */
 
function remove_acf_menu()
{
 
    // provide a list of usernames who can edit custom field definitions here
    $admins = array( 
        'cavell'
    );
 
    // get the current user
    $current_user = wp_get_current_user();
 
    // match and remove if needed
    if( !in_array( $current_user->user_login, $admins ) )
    {
        remove_menu_page('edit.php?post_type=acf');
    }
 
}
 
add_action( 'admin_menu', 'remove_acf_menu', 999 );

// ACF fields code

if( function_exists('register_field_group') ):

register_field_group(array (
	'key' => 'group_5399221bd2316',
	'title' => 'Gallery',
	'fields' => array (
		array (
			'key' => 'field_53992221f6797',
			'label' => 'gallery',
			'name' => 'gallery',
			'prefix' => '',
			'type' => 'gallery',
			'instructions' => 'Upload gallery images here',
			'required' => 0,
			'conditional_logic' => 0,
			'min' => '',
			'max' => '',
			'preview_size' => 'thumbnail',
			'library' => 'all',
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'page_template',
				'operator' => '==',
				'value' => 'gallery-page.php',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
));

register_field_group(array (
	'key' => 'group_539f80761232a',
	'title' => 'Header',
	'fields' => array (
		array (
			'key' => 'field_539f807dbc373',
			'label' => 'logo',
			'name' => 'logo',
			'prefix' => '',
			'type' => 'image',
			'instructions' => 'Upload your logo here',
			'required' => 0,
			'conditional_logic' => 0,
			'return_format' => 'url',
			'preview_size' => 'thumbnail',
			'library' => 'all',
		),
		array (
			'key' => 'field_539f81087ebfd',
			'label' => 'Show site description in header',
			'name' => 'show_site_description_in_header',
			'prefix' => '',
			'type' => 'true_false',
			'instructions' => 'Select to show site description',
			'required' => 0,
			'conditional_logic' => 0,
			'message' => 'Show Description',
			'default_value' => 1,
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'acf-options',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
));

register_field_group(array (
	'key' => 'group_538fdccb058da',
	'title' => 'Home page',
	'fields' => array (
		array (
			'key' => 'field_538fe1db1f44c',
			'label' => 'Home page layout type',
			'name' => 'home_page_layout_type',
			'prefix' => '',
			'type' => 'select',
			'instructions' => 'Choose the layout type',
			'required' => 1,
			'conditional_logic' => 0,
			'choices' => array (
				'slider' => 'Slider',
				'full_slider' => 'Full page slider',
				'masonry' => 'Masonry',
			),
			'default_value' => 'Slider',
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'placeholder' => '',
			'disabled' => 0,
			'readonly' => 0,
		),
		array (
			'key' => 'field_538fe98315040',
			'label' => 'full slider',
			'name' => 'full_slider',
			'prefix' => '',
			'type' => 'repeater',
			'instructions' => 'It is recommended to use image that are normal camera ratio eg. 3:2 and use images that are larger than 1600px wide.

Don\'t use 20 Megapixel images straight from your camera. Otherwise they will take a long time to upload. Down sample them to 2000 - 3000 pixels on the widest edge first. ',
			'required' => 0,
			'conditional_logic' => array (
				array (
					'rule_rule_rule_rule_rule_0' => array (
						'field' => 'field_538fe1db1f44c',
						'operator' => '==',
						'value' => 'full_slider',
					),
				),
			),
			'min' => 1,
			'max' => 6,
			'layout' => 'table',
			'button_label' => 'Add Image',
			'sub_fields' => array (
				array (
					'key' => 'field_538fea8779bfe',
					'label' => 'full slider images',
					'name' => 'full_slider_images',
					'prefix' => '',
					'type' => 'image',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'return_format' => 'array',
					'preview_size' => 'medium',
					'library' => 'uploadedTo',
				),
			),
		),
		array (
			'key' => 'field_538fe99715041',
			'label' => 'slider',
			'name' => 'slider',
			'prefix' => '',
			'type' => 'repeater',
			'instructions' => 'Images uploaded here will be cropped to 1400px x 620px.
For best results crop the images yourself. Images larger will be automatically cropped.',
			'required' => 0,
			'conditional_logic' => array (
				array (
					'rule_rule_rule_0' => array (
						'field' => 'field_538fe1db1f44c',
						'operator' => '==',
						'value' => 'slider',
					),
				),
			),
			'min' => '',
			'max' => '',
			'layout' => 'table',
			'button_label' => 'Add Row',
			'sub_fields' => array (
				array (
					'key' => 'field_53913a8941941',
					'label' => 'slider images',
					'name' => 'slider_images',
					'prefix' => '',
					'type' => 'image',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'column_width' => '',
					'return_format' => 'array',
					'preview_size' => 'slider',
					'library' => 'uploadedTo',
				),
				array (
					'key' => 'field_539a33bbc8c6d',
					'label' => 'slider description',
					'name' => 'slider_description',
					'prefix' => '',
					'type' => 'text',
					'instructions' => 'Enter a description for	the slider here. (Optional)',
					'required' => 0,
					'conditional_logic' => 0,
					'column_width' => '',
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
					'formatting' => 'none',
					'readonly' => 0,
					'disabled' => 0,
				),
				array (
					'key' => 'field_539a33edc8c6e',
					'label' => 'slider link',
					'name' => 'slider_link',
					'prefix' => '',
					'type' => 'page_link',
					'instructions' => 'Choose a page for the slider to link to (Optional)',
					'required' => 0,
					'conditional_logic' => 0,
					'post_type' => array (
						0 => 'post',
						1 => 'attachment',
					),
					'taxonomy' => '',
					'allow_null' => 0,
					'multiple' => 0,
				),
			),
		),
		array (
			'key' => 'field_5397e6066d46c',
			'label' => 'full slider text',
			'name' => 'full_slider_text',
			'prefix' => '',
			'type' => 'wysiwyg',
			'instructions' => 'Adds text over the top of your full page slider images. Leave blank if you don\'t want text on your home page.',
			'required' => 0,
			'conditional_logic' => array (
				array (
					'rule_rule_rule_rule_rule_rule_rule_rule_0' => array (
						'field' => 'field_538fe1db1f44c',
						'operator' => '==',
						'value' => 'full_slider',
					),
				),
			),
			'default_value' => '',
			'toolbar' => 'full',
			'media_upload' => 0,
		),
		array (
			'key' => 'field_538feb2ecf58c',
			'label' => 'masonry text',
			'name' => 'masonry_text',
			'prefix' => '',
			'type' => 'wysiwyg',
			'instructions' => 'Adds text over the top of your masonry layout . Leave blank if you don\'t want text on your home page.',
			'required' => 0,
			'conditional_logic' => array (
				array (
					'rule_rule_rule_rule_rule_rule_rule_0' => array (
						'field' => 'field_538fe1db1f44c',
						'operator' => '==',
						'value' => 'masonry',
					),
				),
			),
			'default_value' => '',
			'toolbar' => 'full',
			'media_upload' => 0,
		),
		array (
			'key' => 'field_538fe9a415042',
			'label' => 'masonry',
			'name' => 'masonry',
			'prefix' => '',
			'type' => 'gallery',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array (
				array (
					'rule_rule_rule_0' => array (
						'field' => 'field_538fe1db1f44c',
						'operator' => '==',
						'value' => 'masonry',
					),
				),
			),
			'min' => '',
			'max' => '',
			'preview_size' => 'masonry-thumb',
			'library' => 'uploadedTo',
		),
		array (
			'key' => 'field_538ff04c8d52b',
			'label' => 'slider flexible content',
			'name' => 'slider_flexible_content',
			'prefix' => '',
			'type' => 'flexible_content',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array (
				array (
					'rule_0' => array (
						'field' => 'field_538fe1db1f44c',
						'operator' => '==',
						'value' => 'slider',
					),
				),
			),
			'button_label' => 'Add Row',
			'min' => '',
			'max' => '',
			'layouts' => array (
				array (
					'key' => '538ff051344bb',
					'name' => 'full_width_text',
					'label' => 'Full width text',
					'display' => 'row',
					'sub_fields' => array (
						array (
							'key' => 'field_538ff0908d52c',
							'label' => 'full width',
							'name' => 'full_width',
							'prefix' => '',
							'type' => 'wysiwyg',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'default_value' => '',
							'toolbar' => 'full',
							'media_upload' => 1,
						),
					),
					'min' => '',
					'max' => '',
				),
				array (
					'key' => '538ff128c8168',
					'name' => 'blog_post_block',
					'label' => 'blog post block',
					'display' => 'table',
					'sub_fields' => array (
						array (
							'key' => 'field_538ff6fffac8c',
							'label' => 'blog posts',
							'name' => 'blog_posts',
							'prefix' => '',
							'type' => 'repeater',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'min' => 3,
							'max' => 3,
							'layout' => 'table',
							'button_label' => 'Add Row',
							'sub_fields' => array (
								array (
									'key' => 'field_538ff7682c61e',
									'label' => 'blog post',
									'name' => 'blog_post',
									'prefix' => '',
									'type' => 'post_object',
									'instructions' => 'Choose a blog post or page to display',
									'required' => 0,
									'conditional_logic' => 0,
									'column_width' => '',
									'post_type' => array (
										0 => 'post',
									),
									'taxonomy' => '',
									'allow_null' => 0,
									'multiple' => 0,
									'return_format' => 'object',
									'ui' => 1,
								),
							),
						),
					),
					'min' => '',
					'max' => '',
				),
				array (
					'key' => '538ff4ebaa5b5',
					'name' => 'image_block',
					'label' => 'Image block',
					'display' => 'row',
					'sub_fields' => array (
						array (
							'key' => 'field_538ff7c52c61f',
							'label' => 'image block repeater',
							'name' => 'image_block_repeater',
							'prefix' => '',
							'type' => 'repeater',
							'instructions' => 'Choose additional image to display',
							'required' => 0,
							'conditional_logic' => 0,
							'min' => 3,
							'max' => 3,
							'layout' => 'table',
							'button_label' => 'Add Image',
							'sub_fields' => array (
								array (
									'key' => 'field_538ff8022c620',
									'label' => 'image',
									'name' => 'image',
									'prefix' => '',
									'type' => 'image',
									'instructions' => '',
									'required' => 0,
									'conditional_logic' => 0,
									'return_format' => 'array',
									'preview_size' => 'thumbnail',
									'library' => 'all',
								),
								array (
									'key' => 'field_538ff81f2c621',
									'label' => 'image link',
									'name' => 'image_link',
									'prefix' => '',
									'type' => 'page_link',
									'instructions' => 'Choose a page or post to link the image to',
									'required' => 0,
									'conditional_logic' => 0,
									'post_type' => array (
										0 => 'post',
										1 => 'page',
									),
									'taxonomy' => '',
									'allow_null' => 0,
									'multiple' => 0,
								),
								array (
									'key' => 'field_538ff8482c622',
									'label' => 'image caption',
									'name' => 'image_caption',
									'prefix' => '',
									'type' => 'text',
									'instructions' => '',
									'required' => 0,
									'conditional_logic' => 0,
									'column_width' => '',
									'default_value' => '',
									'placeholder' => '',
									'prepend' => '',
									'append' => '',
									'maxlength' => '',
									'formatting' => 'none',
									'readonly' => 0,
									'disabled' => 0,
								),
							),
						),
					),
					'min' => '',
					'max' => '',
				),
				array (
					'key' => '538ff1d34ce6f',
					'name' => 'welcome_message',
					'label' => 'welcome message',
					'display' => 'row',
					'sub_fields' => array (
						array (
							'key' => 'field_538ff1e84ce70',
							'label' => 'welcome message text',
							'name' => 'welcome_message_text',
							'prefix' => '',
							'type' => 'text',
							'instructions' => 'Add a large welcome message to your home page',
							'required' => 0,
							'conditional_logic' => 0,
							'column_width' => '',
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'maxlength' => '',
							'formatting' => 'none',
							'readonly' => 0,
							'disabled' => 0,
						),
					),
					'min' => '',
					'max' => 1,
				),
			),
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'page_type',
				'operator' => '==',
				'value' => 'front_page',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => array (
		0 => 'the_content',
		1 => 'excerpt',
		2 => 'custom_fields',
		3 => 'discussion',
		4 => 'comments',
		5 => 'revisions',
		6 => 'slug',
		7 => 'author',
		8 => 'format',
		9 => 'page_attributes',
		10 => 'featured_image',
		11 => 'categories',
		12 => 'tags',
		13 => 'send-trackbacks',
	),
));

register_field_group(array (
	'key' => 'group_53a0f8605df9b',
	'title' => 'Social Media',
	'fields' => array (
		array (
			'key' => 'field_53a0f86c3d5d9',
			'label' => 'Display Social Media Icons',
			'name' => 'display_social_media_icons',
			'prefix' => '',
			'type' => 'true_false',
			'instructions' => 'Check to display Social Media icons',
			'required' => 0,
			'conditional_logic' => 0,
			'message' => '',
			'default_value' => 1,
		),
		array (
			'key' => 'field_53a0f9bae86a1',
			'label' => 'Google+',
			'name' => 'sm_google',
			'prefix' => '',
			'type' => 'text',
			'instructions' => 'Paste the link to your Google+ profile here',
			'required' => 0,
			'conditional_logic' => array (
				array (
					'rule_0' => array (
						'field' => 'field_53a0f86c3d5d9',
						'operator' => '==',
						'value' => '1',
					),
				),
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'formatting' => 'none',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_53a0fa70b498e',
			'label' => 'Facebook',
			'name' => 'sm_facebook',
			'prefix' => '',
			'type' => 'text',
			'instructions' => 'Paste the link to your Facebook page here',
			'required' => 0,
			'conditional_logic' => array (
				array (
					'rule_rule_0' => array (
						'field' => 'field_53a0f86c3d5d9',
						'operator' => '==',
						'value' => '1',
					),
				),
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'formatting' => 'none',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_53a0fab0605e0',
			'label' => 'Twitter',
			'name' => 'sm_twitter',
			'prefix' => '',
			'type' => 'text',
			'instructions' => 'Paste the link to your Twitter profile here',
			'required' => 0,
			'conditional_logic' => array (
				array (
					'rule_rule_rule_0' => array (
						'field' => 'field_53a0f86c3d5d9',
						'operator' => '==',
						'value' => '1',
					),
				),
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'formatting' => 'none',
			'readonly' => 0,
			'disabled' => 0,
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'acf-options',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
));

endif;
?>