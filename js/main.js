// Rock Hammer by Stuff and Nonsense
// Version: <!-- $version -->
// URL: http://stuffandnonsense.co.uk/projects/rock-hammer/
// Version: <!-- $license -->

// Entry point for our JavaScript code
jQuery(document).ready(function($) {


	
	// Navigation Patterns =========
	if (config.navpatterns.responsivenav.use === true) {
		navigation = responsiveNav(config.navpatterns.responsivenav.navelement);
	};


	// Initiaslise tooltips
	if (config.widgets.tooltips.use === true) {
		$(config.widgets.tooltips.container).tooltip({
  			selector: "a[data-toggle=tooltip]"
		});
	}

	// Initialise Popovers
	if (config.widgets.popovers.use === true) {
	    $("a[data-toggle=popover]").popover().click(function(e) {
	        e.preventDefault()
	    });
	}
});